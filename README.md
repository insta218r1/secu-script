## SECU SCRIPT - Infrasec

Le but de ce script est d'installer une suite de logiciels de sécurité. Il à été pensé afin d'être utilisé sur une distribution se basant sur debian (kali-linux, raspbian sur raspberry).

Infrasec utilise [dialog](https://en.wikipedia.org/wiki/Dialog_(software)) comme interface graphique en CLI.

Definition des étapes du script Infrasec :

1. Vérification des programmes installés parmi la liste de ceux à installer 

2. Demande à l'utilisateur quels logiciels il souhaite installer 

3. Recapitulatif des choix de l'utilisateur

4. installation des programmes choisis par l'utilisateur


Explication des étapes du script

    Le script n'étant pas fini notre feuille de route est décrite si dessous.

1. Cette étape sert à avertir l'utilisateur, si des programmes présents dans la liste Infrasec sont déjà installés l'utilisateur le saura, et aura le choix de ne pas installer le ou les programmes.

2. Cette étage à été initialement visualisée par un tableau dialog (--checklist) dans ce tableau sont affichés tous les logiciels qui ne sont pas installés. En cochant ce ou ces programmes et en validant cette étape on enclenchera un script qui installera le ou les programmes souhaités.

3. Cette étape est un simple récapitulatif des choix de l'utilisateur. Tout ce qui à été choisi dans l'étape est référencé ici.

4. Cette étape montre les logs de chaque installation, si une installation échoue le script continuera l'installation d'autres programmes.

15/10/2020 Actuellement le script en est à l'étape 2 


