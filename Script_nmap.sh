#!/bin/bash

#Initialisation des commandes
IpAddr="0"
PortNum="0"
PortsNum="0"
choix_1=""
reponse_webmin="n"
numero_port_webmin="10000"
val="0"
repok="nok"
user=$( whoami)
echo $user
mymachin=$( ip a | egrep global | awk '{print $2}' | awk -F/ ' {print $1}')

Menu1="
---- MENU ----\n
\n
Que souhaitez vous faire ?\n
\n
<1> Scanner le reseau\n
<2> Scanner une machine\n
\n
<3> Installer Webmin\n
<4> Installer Guacamole\n
<5> Quitter\n
\n
";
	
Menu2="
----- SCANNE SUR LE RESEAU -----\n
\n
<1>  GLOBAL\n
<2>  GLOBAL SILENCIEUX\n
<3>  SELON UN PORT\n
<4>  LISTE DE PORTS\n
\n
<5>  Q = Quitter\n
";

Menu3="
----- SCANNE SUR UNE MACHINE -----\n
\n
<1>  IDENTIFICATION DE L'OS\n
<2>  SCANNE D'UN PORT\n
<3>  SCANNE D'UN ORDINATEUR (EN MODE VANILLA)\n
<4>  SCANNE D'UN ORDINATEUR (EN MODE SILENCIEUX : TCP NULL)\n
<5>  SCANNE D'UN ORDINATEUR (EN MODE SILENCIEUX : TCP SYN)\n
<6>  SCANNE D'UN ORDINATEUR (EN MODE SILENCIEUX : TCP FIN)\n
<7>  SCANNE D'UN ORDINATEUR (EN MODE SILENCIEUX : TCP Xmas)\n
\n
<8>  Q = Quitter\n
";

clear

if [ $user = "root" ]; then

while :
do

	echo -e $Menu1
	read choix_1;
	clear
	case $choix_1 in
		1) while :
		do
			echo -e $Menu2
			read choix_2
			echo
			case $choix_2 in
				1) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse reseau avec son masque (exemple : 172.16.12.0/24)" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]).(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]).(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]).(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])/([8-9]|[1-2][0-9]|3[0-2])$ ]];then
          repok="ok"
			echo $IpAddr
					nmap -Pn $IpAddr ;
					fi
          done  
          exit 0;;
					
				2) echo "Veuillez entrer l'adresse reseau avec son masque (exemple : 172.16.12.0/24)" ;
					read IpAddr;
					nmap -sS -Pn $IpAddr ;
					exit 0;;
			
				3) echo "Veuillez entrer l'adresse reseau avec son masque (exemple : 172.16.12.0/24)";
					read IpAddr;
					echo "Veuillez entrer le port vise";
					read PortsNum;
					nmap -p $PortsNum $IpAddr ;
					exit 0;;
			
				4) echo "Veuillez entrer l'adresse ip de la machine cible (exemple : 172.16.12.0/24)";
					read IpAddr;
					echo "Veuillez entrer les numeros de ports souhaites separes par une virgule (exemple : 80,443,etc.)" ;
					read PortsNum;
					nmap -p $PortsNum ;
					exit 0;;
			
				5 | Q | q ) echo "L'utilisateur $USER a quitte le programme" ; echo ; exit 0 ;;
			
				*) echo "Erreur saisi Menu 1";;
			
			esac
		done;;
	

		2) while :
		do
			echo -e $Menu3
			read choix_3
			echo
			case $choix_3 in
				1) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse reseau avec son masque (exemple : 172.16.12.0/24)" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]];then
          repok="ok"
					nmap -O -Pn $IpAddr ;
					fi
          done  
          exit 0;;
			
				2) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse ip de la machine cible " ;
					read IpAddr;
          echo "Veuillez entrer le numero de port souhaite";
          read PortNum;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]] && [ -n $PortNum ] && [[ "$PortNum" =~ ^[0-9]+$ ]] && [ $PortNum -le 65535 ] && [ $PortNum -ge 1 ];then
          repok="ok"
					nmap -p $PortNum $IpAddr ;
          else echo " Erreur de saisie "
          fi
          done
					exit 0;;
			
				3) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse ip de la machine cible" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]];then
          repok="ok"
					nmap -Pn $IpAddr ;
					fi
          done  
          exit 0;;
			
				4) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse ip de la machine cible" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]];then
          repok="ok"
					nmap -sN -Pn $IpAddr ;
					fi
          done  
          exit 0;;
               
        5) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse ip de la machine cible" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]];then
          repok="ok"
					nmap -sS -Pn $IpAddr ;
					fi
          done  
          exit 0;;
        
        6) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse ip de la machine cible" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]];then
          repok="ok"
					nmap -sF -Pn $IpAddr ;
					fi
          done  
          exit 0;;
        
        7) while [ "$repok" != 'ok' ]
          do
          echo "Veuillez entrer l'adresse ip de la machine cible" ;
					read IpAddr;
          if [[ "$IpAddr" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[1-9][0-9]?)$ ]];then
          repok="ok"
					nmap -sX -Pn $IpAddr ;
					fi
          done  
          exit 0;;

				8 | Q | q ) echo "L'utilisateur $USER a quitte le programme" ; 
					echo ; 
					exit 0 ;;
					
				*) echo "Erreur saisi menu principal";;
			esac
		done;;
		
    3)  val=sudo cat /etc/apt/sources.list | egrep "deb http://download.webmin.com/download/repository sarge contrib" | wc -l
        if [ "$val" = "0" ];then
          echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
          wget http://www.webmin.com/jcameron-key.asc
          apt-key add jcameron-key.asc
        fi
        echo " Veuillez patienter pendant l'installation ;D "
        apt-get install apt-transport-https -y >> log.txt
        apt-get update >> log.txt
        apt-get install webmin -y >> log.txt
        echo "L'application a ete installe "
        echo "Voulez-vous modifier le port de l'interface webmin ? (10000 par defaut) "
        read reponse_webmin
        if [ $reponse_webmin = "yes" ] || [ $reponse_webmin = "y" ] || [ $reponse_webmin = "oui" ] || [ $reponse_webmin = "o" ];then
          echo "Quel numero de port voulez-vous affecter � l'interface webmin?"
          read numero_port_webmin
          while [ "$repok" != 'ok' ]
          do
          if [ -n $numero_port_webmin ] && [[ "$numero_port_webmin" =~ ^[0-9]+$ ]] && [ $numero_port_webmin -le 65535 ] && [ $numero_port_webmin -ge 1 ];then
          repok="ok"
          sed -i -e "s/port=10000/port=$numero_port_webmin/g" "/etc/webmin/miniserv.conf"
          /etc/init.d/webmin restart >> log.txt
	 echo "Votre Interface Webmin est accessible sur https://$mymachin:$numero_port_webmin"
          fi
          done
        fi
        exit 0 ;;
        
    4)  apt install -y git >> log.txt ;
        git clone https://github.com/PAPAMICA/scripts >> log.txt;
        cd scripts/debian/guacamole;
        chmod +x debian_install_guacamole.sh;
        echo "Veuillez patienter, l'application est en cours d'installation"
        ./debian_install_guacamole.sh >> log.txt;
        echo "L'application a ete installe "
        #echo "Voulez-vous modifier le port de l'interface webmin ? (10000 par defaut) "
        #read reponse_webmin
        #if [ $reponse_webmin = "yes" ] || [ $reponse_webmin = "y" ] || [ $reponse_webmin = "oui" ] || [ $reponse_webmin = "o" ];then
         # echo "Quel numero de port voulez-vous affecter � l'interface webmin?"
          #read numero_port_webmin
          #while [ $repok != 'ok' ]
          #do
          #if [ -n $numero_port_webmin ] && [[ "$numero_port_webmin" =~ ^[0-9]+$ ]] && [ $numero_port_webmin -le 65535 ] && [ $numero_port_webmin -ge 1 ];then
          #repok="ok"
          #sed -i -e "s/port=10000/port=$numero_port_webmin/g" "/etc/webmin/miniserv.conf"
          #fi
        #fi
         # done      
        exit 0 ;;
   
		5) echo "L'utilisateur $USER a quitte le programme";
      exit 0;;
	
	esac
done

else
	clear;
	echo " Connecter vous en tant qu'admiistrateur pour utiliser cette application."
fi
